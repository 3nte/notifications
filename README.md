#Permisos
Es necesario obtener los permisos de la aplicacion para ver notificaciones
```NUserNotificationCenter.current().requestAuthorization
```

#Alertas
* alert – Display the alert window and text of the notification
* sound – Plays a sound or vibrates. On Apple Watch uses a haptic.
* badge – Places the red dot of the app to give a count of notifications from the app.

# Triggers 
Son los eventos que se desencadenaran cuando se inicialice una notificacion
Pueden ser de tipo:
* tiempos
* fechas
* locations

#CategoryIdentifier
Nos permite asociar acciones hacia la notificacion.

```content.categoryIdentifier = "message"
``
