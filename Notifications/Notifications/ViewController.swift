//
//  ViewController.swift
//  Notifications
//
//  Created by Patricio Chavez on 7/3/17.
//  Copyright © 2017 Patricio Chavez. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {

    var isGrantedNotificationAccess:Bool = false
    var timetoRemember:Double = 10
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert,.sound,.badge],
            completionHandler: { (granted,error) in
                self.isGrantedNotificationAccess = granted
            }
        )
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func send10SecNotification(_ sender: UIButton) {
        notificacion()
    }
    
    func notificacion(){
        if isGrantedNotificationAccess{
            //add notification code here
            
            let aceptar = UNNotificationAction(identifier: "ACEPTAR_ACTION",
                                               title: "Aceptar",
                                               options: UNNotificationActionOptions(rawValue: 0))
            
            let category = UNNotificationCategory(identifier: "category-request", actions: [aceptar], intentIdentifiers: [], options: [])
            UNUserNotificationCenter.current().setNotificationCategories([category])
            
            //Set the content of the notification
            let content = UNMutableNotificationContent()
            content.title = "10 Second Notification Demo"
            content.subtitle = "From MakeAppPie.com"
            content.body = "Notification after 10 seconds - Your pizza is Ready!!"
            content.categoryIdentifier = "category-request"
            
            
            //Set the trigger of the notification -- here a timer.
            let trigger = UNTimeIntervalNotificationTrigger(
                timeInterval: timetoRemember,
                repeats: false)
            
            //Set the request for the notification from the above
            let request = UNNotificationRequest(
                identifier: "10.second.message",
                content: content,
                trigger: trigger
            )
            
            //Add the notification to the currnet notification center
            UNUserNotificationCenter.current().add(
                request, withCompletionHandler: nil)
            
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //1. Si el usuario pulsa en Recordar más tarde, incremento el tiempo a 60 segs.
        if response.actionIdentifier == "ACEPTAR_ACTION" {
            
            timetoRemember = 20
            notificacion()
            
        }
    }

}

